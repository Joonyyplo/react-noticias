import React from "react";
import Noticia from "./Noticia";

const ListaNoticias = props => {
  return (
    <div className="container backgroundNoticias border border-top-0 border-warning rounded-bottom py-5 mb-5">
      <div className="row">
        {props.noticias.map(item => (
          <Noticia articulo={item} key={item.url} />
        ))}
      </div>
    </div>
  );
};

export default ListaNoticias;
