import React from "react";

const Titulo = () => {
  return (
    <div className="bg-warning text-center">
      <h1 className="text-light display-4 border-bottom p-4">Noticias</h1>
    </div>
  );
};

export default Titulo;
