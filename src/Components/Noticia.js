import React from "react";

const Noticia = props => {
  //Aqui va javascript puro
  const { source, urlToImage, title, content } = props.articulo; //aqui realizo destructuring de las propiedades

  //Dentro del return ya tengo que usar llaves {}
  return (
    <div className="col-4">
      <div className="card cards backgroundCard border-warning mb-3">
        <div className="card-header p-0">
          <img className="img-fluid" src={urlToImage} />
        </div>
        <div className="card-body">
          <p>{source.name}</p>
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{content}</p>
        </div>
        <div className="card-footer text-center">
          <button className="btn btn-warning">Ver noticia completa</button>
        </div>
      </div>
    </div>
  );
};

export default Noticia;
