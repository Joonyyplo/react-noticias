import React, { Component } from "react";

class Formulario extends Component {
  state = {
    categoria: "",
    pais: ""
  };

  componentDidMount() {
    this.setState({
      categoria: "general",
      pais: "ar"
    });
  }

  handleChange = e => {
    this.setState(
      {
        ...this.state,
        [e.target.name]: e.target.value //Le asigno a la propiedad 'name' el valor (value) que figura en el input actualmente y lo guardo en el state
      },
      () => {
        this.props.consultas(this.state.categoria, this.state.pais); //Reemplazo los datos de la url en el componente 'app' por los valores ingresados en el state del componente 'formulario'
      }
    );
  };

  render() {
    return (
      <div className="container backgroundForm border border-warning rounded-top mt-5 py-5 px-">
        <div className="form-group m-0">
          <div className="row">
            <label className="p-0 col-md-3 d-flex justify-content-center align-items-center lead m-0">
              Pais:
            </label>
            <select
              onChange={this.handleChange}
              name="pais"
              multiple=""
              className="form-control col-md-8 border-warning"
              id="exampleSelect2"
            >
              <option value="ar">Argentina</option>
              <option value="br">Brazil</option>
              <option value="jp">Japon</option>
              <option value="mx">Mexico</option>
              <option value="ru">Rusia</option>
              <option value="us">Estados Unidos</option>
              <option value="ch">Chile</option>
            </select>
          </div>
          <div className="row mt-2">
            <label className="p-0 col-md-3 d-flex justify-content-center align-items-center lead m-0">
              Categoria:
            </label>
            <select
              onChange={this.handleChange}
              name="categoria"
              multiple=""
              className="form-control col-md-8 border-warning"
              id="exampleSelect2"
            >
              <option value="general">General</option>
              <option value="business">Mercado</option>
              <option value="entertainment">Entretenimiento</option>
              <option value="health">Salud</option>
              <option value="science">Ciencia</option>
              <option value="sports">Deporte</option>
              <option value="technology">Tecnologia</option>
            </select>
          </div>
        </div>
      </div>
    );
  }
}

export default Formulario;
