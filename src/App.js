import React, { Component } from "react";
import Titulo from "./Components/Titulo";
import Fomulario from "./Components/Formulario";
import ListaNoticias from "./Components/ListaNoticias";
import "./bootstrap.min.css";
import "./App.css";

class App extends Component {
  //1- Primero va declarado el state
  state = {
    arregloNoticias: []
  };

  //2- Aqui van los siclos de vida
  componentDidMount() {
    this.consultas();
  }

  //3- Aqui van mis metodos
  consultas = async (categoria = "general", pais = "ar") => {
    const url = `https://newsapi.org/v2/top-headlines?country=${pais}&category=${categoria}&apiKey=de26e1ffdf4744b6aeaffb079815dcaf`;

    const respuesta = await fetch(url); // conecto la url con una API
    const datosListos = await respuesta.json();

    console.log(datosListos.articles);

    this.setState({
      arregloNoticias: datosListos.articles
    });
  };
  //4- Aqui va el render

  render() {
    return (
      <div>
        <Titulo />
        <Fomulario consultas={this.consultas} />
        <ListaNoticias noticias={this.state.arregloNoticias} />
      </div>
    );
  }
}

export default App;
